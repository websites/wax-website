---
title: "Editor Properties and tool groups"
date: 2020-07-06T10:20:10+02:00
draft: false
part: 2
---

## All available editor properties

```javascript
<Wax
//sets cursor in the begging of the document
autoFocus 
// Ex. will display in browser's console the editor's source
onChange={source => console.log(source)}
// the actual HTML content of the editor
value="<p> this is some content</p>" 
//used for uploading images (should return a promise with the actual file path)
fileUpload={file => renderImage(file)}
// a placeholder used for empty documents
placeholder="Type Something..."
// adds on the editor anything from new services, tools, Pmpplugins etc. 
config={config} 
// editor in in read-only mode
readonly
// on focus lost
onBlur
// used to create your own Layout using React components
layout={EditoriaLayout}
// enables track changes
TrackChange
// dev-tools (https://github.com/d4rkr00t/prosemirror-dev-tools)
debug; 
/>
```

## Render Image basic Example

```javascript
const renderImage = file => {
  const reader = new FileReader();
  return new Promise((accept, fail) => {
    reader.onload = () => accept(reader.result);
    reader.onerror = () => fail(reader.error);
    // Some extra delay to make the asynchronicity visible
    setTimeout(() => reader.readAsDataURL(file), 150);
  });
};
```

## All currently availabe Services into config file

```javascript
import { emDash, ellipsis } from "prosemirror-inputrules";
import { columnResizing, tableEditing } from "prosemirror-tables";
import {
  AnnotationToolGroupService,
  ImageService,
  PlaceholderService,
  InlineAnnotationsService,
  LinkService,
  ListsService,
  ListToolGroupService,
  TablesService,
  TableToolGroupService,
  BaseService,
  BaseToolGroupService,
  DisplayBlockLevelService,
  DisplayToolGroupService,
  ImageToolGroupService,
  TextBlockLevelService,
  TextToolGroupService,
  NoteService,
  NoteToolGroupService,
  TrackChangeService,
  CommentsService
} from "wax-prosemirror-services";

import { WaxSelectionPlugin } from "wax-prosemirror-plugins";

import invisibles, {
  space,
  hardBreak,
  paragraph
} from "@guardian/prosemirror-invisibles";

export default {
  MenuService: [
    {
      templateArea: "topBar",
      toolGroups: [
        "Base",
        {
          name: "Annotations",
          more: ["Superscript", "Subscript", "SmallCaps"]
        },
        "Notes",
        "Lists",
        "Images",
        "Tables"
      ]
    },
    {
      templateArea: "leftSideBar",
      toolGroups: ["Display", "Text"]
    }
  ],

  RulesService: [emDash, ellipsis],
  ShortCutsService: {},

  PmPlugins: [
    columnResizing(),
    tableEditing(),
    invisibles([hardBreak()]),
    WaxSelectionPlugin
  ],

  // Always load first CommentsService and LinkService,
  //as it matters on how PM treats nodes and marks
  services: [
    new DisplayBlockLevelService(),
    new DisplayToolGroupService(),
    new TextBlockLevelService(),
    new TextToolGroupService(),
    new ListsService(),
    new TrackChangeService(),
    new CommentsService(),
    new LinkService(),
    new PlaceholderService(),
    new ImageService(),
    new InlineAnnotationsService(),
    new TablesService(),
    new BaseService(),
    new BaseToolGroupService(),
    new NoteService(),
    new TableToolGroupService(),
    new ImageToolGroupService(),
    new AnnotationToolGroupService(),
    new NoteToolGroupService(),
    new ListToolGroupService()
  ]
};
```

## Current Tools and Toolgroups

-   *Base Tool group*: `undo`, `redo`, `save`
-   *Inline Annotations Tool group*: `strong`, `italic`, `link`,
    `strikethrough`, `underline`, `subscript`, `superscript`,
    `small caps`
-   *Lists Tool group*: `numbered list`, `bullet list`
-   *Image Tool group*: `image`
-   *Table Tool group*: `create table`, edit table dropdown that
    includes: `insert/delete row`, `insert/delete column`
    ,`merge cells`, `split cell`, `delete table`,
    `Toggle header column`, `Toggle header row`
-   *Display Tool group*: `Title`, `Author`, `Subtitle`,
    `Epigraph Prose`, `Epigraph Poetry`, `Heading 1`, `Heading 2`,
    `Heading 3`
-   *Text Tool group*: `Paragraph`, `Paragraph Continued`,
    `Extract Prose`, `Extract Poetry`, `Source Note`, `Block Quote`
-   *Notes Tool group*: `notes`
-   *Comments Tool group*: `comments`