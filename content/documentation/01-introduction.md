---
title: "Introduction"
date: 2020-07-06T10:20:10+02:00
draft: false
part: 1
---

Wax Editor is built against Prosemirror libraries. Check Prosemirror [website](https://prosemirror.net/) and [GitHub repo](https://github.com/ProseMirror) for more information.

## Before Getting Started

Wax depends on the following libraries.

- [React](https://reactjs.org/docs/getting-started.html) for the view(UI)
- [Styled-components](https://styled-components.com/docs) for theming and styling.
- [Inversify.io](http://inversify.io/) as service containers

Notice: Make sure to check them out before starting developing Services, Components, and Theming for Wax. Detailed examples will also follow using the above libraries, but some knowledge is required.

Setting up a full Prosemirror editor 'from scratch', using only the core libraries, requires quite a lot of code.

Wax is broken down into 8 individual packages, which when combined create a full editor, requiring minimum code. On the following overview, we will go through:

1. Using the core libraries to create your own editor.
2. Using all/some of the already build Services (packages) to customize your editor
3. Creating a custom Layout for your needs
4. Configuring the already build packages
5. Extending the functionality by writing your own Service
6. Overview and how you can use certain functionalities provided by Wax, like Overlays, Toolbars, Layouts, and areas, etc.

Latest published versions:

- wax-prosemirror-components\@0.0.10
- wax-prosemirror-core\@0.0.10
- wax-prosemirror-layouts\@0.0.10
- wax-prosemirror-plugins\@0.0.10
- wax-prosemirror-schema\@0.0.10
- wax-prosemirror-services\@0.0.10
- wax-prosemirror-themes\@0.0.10
- wax-prosemirror-utilities\@0.0.10

Notice: All published packages are transpiled*,* so no action is
required from your end to include them in your application.

## Quick Overview of Packages

### Wax Prosemirror Core

Core's role is

- Mount a prosemirror instance
- Initiate default
    [services](https://gitlab.coko.foundation/wax/wax-prosemirror/blob/master/wax-prosemirror-core/src/config/defaultConfig.js )
    - LayoutService
    - SchemaService
    - MenuService
    - RulesService
    - ShortCutsService

A big part of wax-core is the [application layer](https://gitlab.coko.foundation/wax/wax-prosemirror/blob/master/wax-prosemirror-core/src/Application.js ), which is responsible for the application's lifecycle by registering and booting services, merging configs, using the schema, and gathering all prosemirror plugins.

Also holds some default prosemirror plugins that are necessary like the dropCursor, gapCursor, history, and some optional as the placeholder.

```javascript
import { Wax } from "wax-prosemirror-core";
```

`<Wax/>` will mount a Prosemirror instance provided that you would pass the configuration that we will see in a later state.

Once the instance is mounted core's task is to also communicate through `WaxContext` every change that is happening onto Prosemirror's state to the Components.

```javascript
import { WaxContext } from "wax-prosemirror-core";
```

We will go through all of those when we'll create a React Component for our Editor.

### Wax Prosemirror Services

Service providers are the central place of Wax bootstrapping. Your own services, as well as all of Wax\'s core services, are bootstrapped via application provider and are initiated before everything else.But, what do we mean by \"bootstrapped\"? In general, we mean registering things, including registering service container bindings and event listeners. **Service providers are the central place to configure your Application.** We will dive into detail on how you can create your own Service, what are the Boot and Register methods that you find inside a Service and, extra functionalities already build in which you can use. 

### Wax Prosemirror Schema

Holds all the nodes and marks currently supported by Wax. You can either have a node/mark in "Wax node/mark structure" or a default prosemirror node/mark. Later on, we will see how a "Wax node/mark is different" and how you can add your own node/mark through a Service.

### Wax Prosemirror Components

React Components that are used throughout the editor, ranging from buttons to toolbars, overlays, comment discussions, etc. Later on, we will go through on how you can add your components from your Layout inside a service and how you can have access to Prosemirror's State to develop the functionality needed.

### Wax Prosemirror Plugins

Various Prosemirror plugins. Check Prosemirror's documentation on how to create a [Plugin](https://prosemirror.net/docs/ref/#state.Plugin_System). Later on, we will go through the different ways you can pass Prosemirror Plugins into Wax.

### Wax Prosemirror Layouts

Holds different layouts of the editor. Through the Layout Service, you can configure the "areas" of different components so Wax Services know where to render the Components. Later on, we will go into detail on how this is possible.

### Wax Prosemirror Themes

Holds different themes of the editor. Check the options in the [CokoTheme](https://gitlab.coko.foundation/wax/wax-prosemirror/blob/master/wax-prosemirror-themes/src/coko-theme/index.js) for the list of variables and Components you can style.

### Wax Prosemirror Utilities

Various reusable helper methods needed throughout Wax to support the functionality needed. Currently, they are broken down into 3 major [categories](https://gitlab.coko.foundation/wax/wax-prosemirror/tree/master/wax-prosemirror-utilities/src) (Commands, Document Transformations, and Schema helpers)