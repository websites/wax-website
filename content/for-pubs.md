---
title: "Wax for publishers"
date: 2020-07-06T10:20:10+02:00
draft: false
intro: "So Wax is ready to be used as the foundations of the word processor you need to use. Let’s have a look at how Editoria mixes packages in Wax to define the layout and workflows inside the book production platform."

---



{{<feature class="pub">}}
### write what you need
- Surface for text editor 
- footnotes
- basic semantics
- custom semantics
{{</feature>}}

{{<feature class="pub">}}
### Semantics of your own
Quis incididunt nostrud aute dolore cupidatat sunt incididunt id incididunt exercitation dolore cupidatat. Anim sunt occaecat incididunt labore ullamco dolor non enim officia. Do do ex ea ex ipsum qui incididunt commodo sunt.
{{</feature>}}

{{<feature class="pub">}}
### Collaboration in the content
Comments and conversation
track changes support
{{</feature>}}

{{<feature class="pub">}}
### Collaboration on the content
Comments and conversation
track changes support
{{</feature>}}