---
title: "Wax for developers"
date: 2020-07-06T10:20:10+02:00
draft: false
---

{{<feature class="dev">}}
### Developping for Wax 
The first line of the documentation of Prose Mirror says this:  “Setting up a full editor ‘from scratch’, using only the core libraries [from Prose Mirror], requires quite a lot of code”.  Wax is aiming to remove all the complexity of developing an editor from scratch and build the word processor you need.
{{</feature>}}

{{<feature class="dev">}}
### UX/UI designer friendly
Wax offers the possibility to build your own layout and design the editor you need. You can change the location of any tool, from the most basic toolbar to the most complicated feature, without having to build
{{</feature>}}
{{<feature class="dev">}}
### Developer friendly
Wax use a growing list of features separated into packages. Adding or removing features becomes simple plug-in or plug-out those packages.

Some examples of existing packages:

- track changes, to allow editors 
- footnotes
- spell checker
- etc.
{{</feature>}}
{{<feature class="dev">}}
### Reusable theme
Wax includes a theming mecanism that makes it easy to reuse the work you already put in wax in another environment. For example, you can define a theme for the author to write and define another one when the team edits the content, making it more user-centric than any other
{{</feature>}}

{{<feature class="dev">}}
### Testing Wax in its Editoria flavor
Inserting Wax demo here is a full-screen, surround by informations about how it works.
{{</feature>}}
